<div align="center">
  <img src="https://cdn.jsdelivr.net/npm/twemoji@11.3.0/2/svg/1f33f.svg" width="64">
  <h1>My Personal Website</h1>
  <p>This is the main website were you are going to find my projects, about me and how to find me.<p>
    
![per-web-screenshot](https://github.com/GabsEdits/gabs.eu.org/assets/110247388/e1f2c8ab-790d-4032-b1ca-efede90db566)
</div>

## Story
Check the story behind this website on my [blog](https://gabs.eu.org/blog/posts/changing-up-my-website).
