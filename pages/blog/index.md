---
posts:
  - title: Changing Up My Website 
    desc: Why and how I'm redoing my website.
    date: 13/01/2024
    tags:
      - tech
      - web
      - personal
      - devlog
  - title: Behind Aplós
    desc: A new way to make websites, with VitePress.
    date: 04/01/2024
    tags:
      - tech
      - web
      - devlog
      - oss
---

# Blog

Welcome to my blog, a space where I share updates on my latest projects and delve into topics that captivate my interest.

<BlogList />

<script setup lang="ts">
import BlogList from './components/BlogList.vue'
</script>